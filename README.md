To use the new supply script, place down a huron container or a cargo container. The ACE cargo size is automatically set to 25 through the script.

The create_supply_box.sqf and supplyscripts folder have to be in the same format and folder structure to run, it runs similar to the previous supply script just that init field of the supply crate is replaced with [this] execVM "create_supply_box.sqf"; instead of the multiple add action commands.

The file create_supply_box.sqf and folder supplyscripts will lie beside the mission.sqm file in the mission folder.

The ACE actions are self-explanatory and provide hints and system debug messages upon being used.