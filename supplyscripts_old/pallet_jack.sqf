params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "boxloader_pallet_jack";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

[_crate, 2] call ace_cargo_fnc_setSize;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Pallet Jack spawned";