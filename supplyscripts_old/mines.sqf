params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "Box_NATO_AmmoOrd_F";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

private _crateItems = [
    ["ATMine_Range_Mag", 4],
    ["rhsusf_mine_m14_mag", 6]
];

{_crate addItemCargoGlobal [_x select 0, _x select 1]} foreach _crateItems;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Mines Crate spawned";