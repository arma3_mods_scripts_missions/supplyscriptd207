params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "Box_NATO_Ammo_F";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

private _crateItems = [
    ["rhs_mag_30Rnd_556x45_Mk318_Stanag_Pull", 20],
    ["1Rnd_HE_Grenade_shell", 10],
    ["SMA_30Rnd_68x43_BT_IR", 20],
    ["ACE_20Rnd_762x51_Mk316_Mod_0_Mag", 15],
    ["SMA_20Rnd_762x51mm_Mk316_Mod_0_Special_Long_Range_IR", 15]
];

{_crate addItemCargoGlobal [_x select 0, _x select 1]} foreach _crateItems;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Small Arms Crate spawned";