params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "Land_Boxloader_Crate_1";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

private _crateItems = [
    ["rhs_mag_30Rnd_556x45_Mk318_Stanag_Pull", 60],
    ["1Rnd_HE_Grenade_shell", 30],
    ["SMA_30Rnd_68x43_BT_IR", 60],
    ["ACE_20Rnd_762x51_Mk316_Mod_0_Mag", 45],
    ["SMA_20Rnd_762x51mm_Mk316_Mod_0_Special_Long_Range_IR", 45],   
    ["rhs_weap_M136", 12],
    ["ACE_elasticBandage", 100],
    ["ACE_packingBandage", 100],
    ["ACE_quikclot", 100],
    ["ACE_fieldDressing", 100],
    ["ACE_adenosine", 25],
    ["ACE_bloodIV", 25],
    ["ACE_bloodIV_250", 25],
    ["ACE_bloodIV_500", 25],
    ["ACE_bodyBag", 12],
    ["ACE_epinephrine", 25],
    ["ACE_morphine", 50],
    ["kat_painkiller", 50],
    ["ACE_personalAidKit", 15],
    ["ACE_splint", 65],
    ["ACE_surgicalKit", 65],
    ["ACE_tourniquet", 65],
    ["kat_Accuvac", 5],
    ["kat_guedel", 50],
    ["kat_larynx", 50],
    ["kat_stethoscope", 13],
    ["kat_Pulseoximeter", 25],
    ["kat_chestsealt", 25],
    ["kat_AED", 5],
    ["kat_IV_16", 75],
    ["kat_IO_fast", 25],
    ["kat_naloxone", 50],
    ["kat_Phenylephrine", 50],
    ["kat_Nitroglycerin", 50],
    ["kat_Norepinephrine", 50],
    ["kat_Atropine", 50],
    ["kat_Carbonate", 50],
    ["rhsusf_100Rnd_762x51_m61_ap", 25],
    ["rhsusf_200Rnd_556x45_M855_soft_pouch", 25],
    ["200Rnd_65x39_cased_Box", 25],
    ["150Rnd_93x64_Mag", 25],
    ["ACE_Tripod", 5],
    ["ACE_RangeCard", 3],
    ["20rnd_mas_aus_762x51_Mag", 40],
    ["rhsusf_20rnd_762x51_m993_Mag", 40]
];

{_crate addItemCargoGlobal [_x select 0, _x select 1]} forEach _crateItems;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Boxloader Crate spawned";