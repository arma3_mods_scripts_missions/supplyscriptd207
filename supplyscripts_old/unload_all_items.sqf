params ["_target", "_player", "_params"];

_target = _this select 0;

_all_items = _target getVariable ["ace_cargo_loaded", []];

{[_x, _target] call ace_cargo_fnc_unloadItem} foreach _all_items;

_all_items_check = count (_target getVariable ["ace_cargo_loaded", []]);

if (_all_items_check != 0) then {
    systemChat "Items are still left in the ACE cargo";
    _left_items = _target getVariable ["ace_cargo_loaded", []];
    {[_x, _target] call ace_cargo_fnc_unloadItem} foreach _left_items;
};

hint "All items are removed from crate";