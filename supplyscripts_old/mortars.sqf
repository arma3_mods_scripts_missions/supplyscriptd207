params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "BOX_IED_Exp_f";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

private _crateItems = [
    ["ACE_1Rnd_82mm_Mo_HE", 32],
    ["ACE_1Rnd_82mm_Mo_Illum", 8],
    ["ACE_1Rnd_82mm_Mo_HE_LaserGuided", 10],
    ["ACE_1Rnd_82mm_Mo_Smoke", 8]
];

{_crate addItemCargoGlobal [_x select 0, _x select 1]} foreach _crateItems;
[_crate, 1] call ace_cargo_fnc_setSize;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Mortars Crate spawned";