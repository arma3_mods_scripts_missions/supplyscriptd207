params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "Box_Syndicate_Ammo_F";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

private _crateItems = [
    ["rhsusf_100Rnd_762x51_m61_ap", 10],
    ["rhsusf_200Rnd_556x45_M855_soft_pouch", 10],
    ["200Rnd_65x39_cased_Box", 10],
    ["150Rnd_93x64_Mag", 10]
];

{_crate addItemCargoGlobal [_x select 0, _x select 1]} foreach _crateItems;
[_crate, true, [0,2,1], 2] call ace_dragging_fnc_setCarryable;
[_crate, 1] call ace_cargo_fnc_setSize;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Heavy Arms Crate spawned";