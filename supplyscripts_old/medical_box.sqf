params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "ACE_medicalSupplyCrate_advanced";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

private _crateItems = [
    ["ACE_elasticBandage", 40],
    ["ACE_packingBandage", 40],
    ["ACE_quikclot", 40],
    ["ACE_fieldDressing", 40],
    ["ACE_adenosine", 10],
    ["ACE_bloodIV", 10],
    ["ACE_bloodIV_250", 10],
    ["ACE_bloodIV_500", 10],
    ["ACE_bodyBag", 5],
    ["ACE_epinephrine", 10],
    ["ACE_morphine", 20],
    ["kat_painkiller", 20],
    ["ACE_personalAidKit", 6],
    ["ACE_splint", 25],
    ["ACE_surgicalKit", 25],
    ["ACE_tourniquet", 25],
    ["kat_Accuvac", 2],
    ["kat_guedel", 20],
    ["kat_larynx", 20],
    ["kat_stethoscope", 5],
    ["kat_Pulseoximeter", 10],
    ["kat_chestsealt", 10],
    ["kat_AED", 2],
    ["kat_IV_16", 30],
    ["kat_IO_fast", 10],
    ["kat_naloxone", 20],
    ["kat_Phenylephrine", 20],
    ["kat_Nitroglycerin", 20],
    ["kat_Norepinephrine", 20],
    ["kat_Atropine", 20],
    ["kat_Carbonate", 20]
];

{_crate addItemCargoGlobal [_x select 0, _x select 1]} foreach _crateItems;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Medical Crate spawned";