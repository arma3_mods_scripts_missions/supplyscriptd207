params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "Box_NATO_WpsSpecial_F";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

private _crateItems = [
    ["rhs_fgm148_magazine_AT", 4]
];

{_crate addItemCargoGlobal [_x select 0, _x select 1]} foreach _crateItems;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "FGM Weapons Crate spawned";