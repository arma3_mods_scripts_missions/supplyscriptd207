params ["_target"];

resupply = _this select 0;

[resupply, 25] call ace_cargo_fnc_setSpace;

_small_arms_spawn = compile preprocessFile "supplyscripts\small_arms.sqf";
_standard_section_box_spawn = compile preprocessFile "supplyscripts\standard_section_box.sqf";
_medical_box_spawn = compile preprocessFile "supplyscripts\medical_box.sqf";
_demo_blocks_spawn = compile preprocessFile "supplyscripts\demo_block.sqf";
_mortars_spawn = compile preprocessFile "supplyscripts\mortars.sqf";
_mines_spawn = compile preprocessFile "supplyscripts\mines.sqf";
_lat_spawn = compile preprocessFile "supplyscripts\lat.sqf";
_maaws_spawn = compile preprocessFile "supplyscripts\maaws.sqf";
_titan_spawn = compile preprocessFile "supplyscripts\titan.sqf";
_tow_static_spawn = compile preprocessFile "supplyscripts\tow_static.sqf";
_tow_ammo_spawn = compile preprocessFile "supplyscripts\tow_ammo.sqf";
_weapons_fgm_spawn = compile preprocessFile "supplyscripts\weapons_fgm.sqf";
_heavy_arms_spawn = compile preprocessFile "supplyscripts\heavy_arms.sqf";
_static_mk19_spawn = compile preprocessFile "supplyscripts\static_mk19.sqf";
_weapons_box_spawn = compile preprocessFile "supplyscripts\weapons_box.sqf";
_static_m2_spawn = compile preprocessFile "supplyscripts\static_m2.sqf";
_static_m2_ammo_spawn = compile preprocessFile "supplyscripts\static_m2_ammo.sqf";
_pallet_jack_spawn = compile preprocessFile "supplyscripts\pallet_jack.sqf";
_boxloader_spawn = compile preprocessFile "supplyscripts\boxloader.sqf";
_berets_spawn = compile preprocessFile "supplyscripts\berets.sqf";
_unload_all_items_spawn = compile preprocessFile "supplyscripts\unload_all_items.sqf";


// Starting the tree
_resup = ["resupply_tree", "Choose Resupply Crate", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions"], _resup] call ace_interact_menu_fnc_addActionToObject;

// Basic Stuff
_small_arms = ["small_arms", "<t color='#F52E00'>Small Arms</t>", "", _small_arms_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree"], _small_arms] call ace_interact_menu_fnc_addActionToObject;

_standard_section_box = ["standard_section_box", "<t color='#F00CD8'>Standard Section Box</t>", "", _standard_section_box_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree"], _standard_section_box] call ace_interact_menu_fnc_addActionToObject;

// Medical Crate
_medical = ["medical_box", "<t color='#F0210C'>Medical</t>", "",_medical_box_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree"], _medical] call ace_interact_menu_fnc_addActionToObject;

// Explosives and Mortars Sub-Section
_demo_ordanance = ["demo_ordanance", "Explosives/Mortars", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainAction", "resupply_tree"], _demo_ordanance] call ace_interact_menu_fnc_addActionToObject;

_demo_blocks = ["_spawn_demo", "<t color='#4FA64F'>Demo</t>", "", _demo_blocks_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainAction", "resupply_tree", "demo_ordanance"], _demo_blocks] call ace_interact_menu_fnc_addActionToObject;

_mortars = ["_spawn_mortars", "<t color='#4FA64F'>Mortars</t>", "", _mortars_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "demo_ordanance"], _mortars] call ace_interact_menu_fnc_addActionToObject;

_mines = ["_spawn_mines", "<t color='#4FA64F'>Mines</t>", "", _mines_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "demo_ordanance"], _mines] call ace_interact_menu_fnc_addActionToObject;

// Anti-Tank Sub-Section
_anti_tank = ["anti_tank", "Anti-Tank", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree"], _anti_tank] call ace_interact_menu_fnc_addActionToObject;

_lat = ["lat", "<t color='#ACEC24'>Light AT</t>", "", _lat_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "anti_tank"], _lat] call ace_interact_menu_fnc_addActionToObject;

_maaws = ["maaws", "<t color='#ACEC24'>MAAWS</t>", "", _maaws_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "anti_tank"], _maaws] call ace_interact_menu_fnc_addActionToObject;

_titan = ["titan", "<t color='#15ECEC'>Titan</t>", "", _titan_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "anti_tank"], _titan] call ace_interact_menu_fnc_addActionToObject;

_tow_static = ["tow_static", "<t color='#4FA64F'>TOWS Static</t>", "", _tow_static_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "anti_tank"], _tow_static] call ace_interact_menu_fnc_addActionToObject;

_tow_ammo = ["tow_ammo", "<t color='#4FA64F'>TOW Ammo</t>", "", _tow_ammo_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "anti_tank"], _tow_ammo] call ace_interact_menu_fnc_addActionToObject;

_weapons_fgm = ["weapons_fgm", "<t color='#EC0B4F'>FGM Crates</t>", "", _weapons_fgm_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "anti_tank"], _weapons_fgm] call ace_interact_menu_fnc_addActionToObject;

// Heavy Weapons Section
_heavy_weapons = ["heavy_weapons", "Heavy Weapons", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree"], _heavy_weapons] call ace_interact_menu_fnc_addActionToObject;

_heavy_arms = ["heavy_arms", "<t color='#4FA64F'>MMG</t>", "", _heavy_arms_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "heavy_weapons"], _heavy_arms] call ace_interact_menu_fnc_addActionToObject;

_static_mk19 = ["static_mk19", "<t color='#4FA64F'>Mk19 Box</t>", "", _static_mk19_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "heavy_weapons"], _static_mk19] call ace_interact_menu_fnc_addActionToObject;

_weapons_box = ["weapons_box", "<t color='#0CEC35'>Weapons .338</t>", "", _weapons_box_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "heavy_weapons"], _weapons_box] call ace_interact_menu_fnc_addActionToObject;

_static_m2 = ["static_m2", "<t color='#4FA64F'>M2 Box</t>", "", _static_m2_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "heavy_weapons"], _static_m2] call ace_interact_menu_fnc_addActionToObject;

_static_m2_ammo = ["static_m2_ammo", "<t color='#4FA64F'>M2 Ammo</t>", "", _static_m2_ammo_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "heavy_weapons"], _static_m2_ammo] call ace_interact_menu_fnc_addActionToObject;

// Miscellaneous Stuff
_misc_stuff = ["misc_stuff", "Misc Stuff", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0 ,["ACE_MainActions", "resupply_tree"], _misc_stuff] call ace_interact_menu_fnc_addActionToObject;

_pallet_jack = ["pallet_jack", "<t color='#4FA64F'>Pallet Jack</t>", "", _pallet_jack_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "misc_stuff"], _pallet_jack] call ace_interact_menu_fnc_addActionToObject;

_boxloader = ["boxloader", "<t color='#4FA64F'>Boxloader</t>", "", _boxloader_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "misc_stuff"], _boxloader] call ace_interact_menu_fnc_addActionToObject;

_berets = ["berets", "<t color='#4FA64F'>Berets</t>", "", _berets_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions", "resupply_tree", "misc_stuff"], _berets] call ace_interact_menu_fnc_addActionToObject;

_unload_all_items = ["unload_all_items", "Unload All items", "", _unload_all_items_spawn, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions"], _unload_all_items] call ace_interact_menu_fnc_addActionToObject;
