params ["_target"];

_sandbag_spawn = compile preprocessFile "fortifyobjects\sandbag_spawn.sqf";

systemChat "Testing exevVM on player";

_player_fortify = _this select 0;

// Starting the fortify script
_fortify = ["action_fortify", "207 Fortify", "", {nil}, {[_player_fortify] call ace_fortify_fnc_canFortify;}] call ace_interact_menu_fnc_createAction;
[_player_fortify, 1, ["ACE_SelfActions"], _fortify] call ace_interact_menu_fnc_addActionToObject;

_fortify_sandbag = ["action_fortify_sandbag", "Create Sandbag", "", _sandbag_spawn, {true}] call ace_interact_menu_fnc_createAction;
[_player_fortify, 1, ["ACE_SelfActions", "action_fortify"], _fortify_sandbag] call ace_interact_menu_fnc_addActionToObject; 