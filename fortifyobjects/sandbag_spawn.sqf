params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_cost = 5;

_classname = "Land_BagFence_Short_F";

_budget = missionNamespace getVariable "budget_207";

_budget = _budget - _cost;

missionNamespace setVariable ["budget_207", _budget, true];

[_target, _player, [west, _classname]] call ace_fortify_fnc_deployObject;